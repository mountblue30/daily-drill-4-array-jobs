// 1. Find all card numbers whose sum of all the even position digits is odd.
// 2. Find all cards that were issued before June.
// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
// 4. Add a new field to each card to indicate if the card is valid or not.
// 5. Invalidate all cards issued before March.
// 6. Sort the data into ascending order of issue date.
// 7. Group the data in such a way that we can identify which cards were assigned in which months.

// Use method chaining to solve #3, #4, #5, #6 and #7.

let cards = require("./dataset")
function findAllCards(cards) {
    return cards.map((card) => {
        let sum = 0
        card.card_number.split("").map((number, index) => {
            if ((index + 1) % 2 === 0) {
                sum += +number;
            }
        })
        if (sum % 2 !== 0) {
            return card;
        }
    }).filter((cards) => {
        return cards !== undefined;
    })

}
console.log(findAllCards(cards));



function findBeforeJune(cards) {
    return cards.filter((card) => {
        return +(card.issue_date.split("/")[0]) < 6;
    })
}

console.log(findBeforeJune(cards));


function addCVV(cards) {
    return cards.map((element) => {
        element["CVV"] = +`${(Math.random() * 10000)}`.slice(0, 3);
        return cards;
    })
}

console.log(addCVV(cards));


function addField(cards) {
    return cards.map((card) => {
        card["validity"] = true;
        return card;
    })

}

console.log(addField(cards));


function isInvalidCards(cards) {
    return cards.map((card) => {
        if (card.issue_date.split("/")[0] < 3) {
            card["validity"] = false;
        } else {
            card["validity"] = true;
        }

        return card;
    })

}

console.log(isInvalidCards(cards));



function sortDate(cards) {
    return cards.sort((first, second) => {
        return +first.issue_date.split("/")[0] - +second.issue_date.split("/")[0];
    })
}

console.log(sortDate(cards));




function groupData(cards) {
    months = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }
    return cards.reduce((acc, curr) => {
        if (acc[months[curr["issue_date"].split("/")[0]]] === undefined) {
            acc[months[curr["issue_date"].split("/")[0]]] = []
        }
        acc[months[curr["issue_date"].split("/")[0]]].push(curr);
        return acc;
    }, {})
}

console.log(groupData(cards));